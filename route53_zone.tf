# Local VPC DNS zone
resource "aws_route53_zone" "vpc_prunux_net" {
  name    = "vpc.${var.customer_domain_name}"
  comment = "managed by Terraform and Lambda"

  vpc {
    vpc_id = "${aws_vpc.prunux-mgt.id}"
  }

  tags {
    Customer = "${var.customer_name}"
    VPC      = "${module.common.vpcs[var.env]}"
  }
}

resource "aws_route53_record" "vpc-ns" {
  zone_id = "${aws_route53_zone.vpc_prunux_net.zone_id}"
  name    = "vpc.${var.customer_domain_name}"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.vpc_prunux_net.name_servers.0}",
    "${aws_route53_zone.vpc_prunux_net.name_servers.1}",
    "${aws_route53_zone.vpc_prunux_net.name_servers.2}",
    "${aws_route53_zone.vpc_prunux_net.name_servers.3}",
  ]
}
