# create special cloud-init file
data "template_file" "bastion-host" {
  count    = "${var.create_bastion_host ? 1 : 0}"
  template = "${module.common.cloud_init_bastion_host}"

  vars = {
    hostname = "${var.customer_name_aws}-${var.aws_region}-${var.env}-bastion"
    fqdn     = "${var.customer_name_aws}-${var.aws_region}-${var.env}-bastion.${var.customer_domain_name}"
  }
}

# create bastion host
resource "aws_instance" "bastion-host" {
  count                       = "${var.create_bastion_host ? 1 : 0}"
  ami                         = "${module.common.ubuntu_bionic_amd64_ami}"
  availability_zone           = "${module.common.availability_zones[count.index % length(module.common.availability_zones)]}"
  instance_type               = "t3.nano"
  key_name                    = "${var.aws_key_name}"
  subnet_id                   = "${element(aws_subnet.prunux-mgt-public.*.id, count.index % length(module.common.availability_zones))}"
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.bastion-host.name}"
  ebs_optimized               = true

  credit_specification = {
    cpu_credits = "standard"
  }

  root_block_device {
    delete_on_termination = false
    volume_type           = "gp2"
    volume_size           = "${var.bastion_host_disk_size}"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prunux_mgt.id}",
    "${aws_security_group.bastion-host.id}",
  ]

  tags = {
    Name        = "${var.customer_name_aws}-${var.aws_region}-${var.env}-bastion"
    Environment = "${var.env}"
    VPC         = "${module.common.vpcs[var.env]}"
    Customer    = "${var.customer_name}"
    Customer_ID = "${var.customer_id}"
  }

  user_data = "${data.template_file.bastion-host.rendered}"

  lifecycle {
    ignore_changes = ["ami", "user_data"]
  }
}
